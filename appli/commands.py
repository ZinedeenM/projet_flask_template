import click
from .app import app, db



@app.cli.command()
def create_db():
    '''Crée la base de donnée'''
    db.create_all()


