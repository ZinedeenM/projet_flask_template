


read -p "utilisez vous un ordinateur connecté au réseau de l'iut ?[y/n]" reponse

if echo "$reponse" | grep -iq "^y" ;then
export http_proxy="http://wwwcache.univ-orleans.fr:3128/"
export https_proxy="http://wwwcache.univ-orleans.fr:3128/"
fi

read -p "voulez vous (re)créer le virtualenv ?[y/n]" reponse

if echo "$reponse" | grep -iq "^y" ;then

rm -rf venv/
virtualenv -p python3 venv/
source venv/bin/activate
pip3 install -r requirements.txt
deactivate
fi

source venv/bin/activate
echo "Vous pouvez lancer votre application en faisant : flask run"